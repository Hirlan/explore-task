# explore-task

-Kafka -> kafka-sample-code
-Drools -> ruleengine



## Getting started
kafka-sample-code
```
--run application on local repository

--open postman and hit cURL bellow

curl --location 'http://localhost:8080/api/v1/messages' \
--header 'Content-Type: application/json' \
--data '{
    "message":"Kafka API "
}'
```
##
ruleengine
````
--run application on local repository

--open postman and hit cURL bellow

curl --location 'http://localhost:8080/get-discount' \
--header 'Content-Type: application/json' \
--data '{"customerNumber":"0876899863",
"age":10,
"amount":20,
"customerType":"NEW"}'
```