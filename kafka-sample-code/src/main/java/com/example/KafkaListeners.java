package com.example;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {
    @KafkaListener(topics = "kafka-message-topic",groupId = "groupId")
    void listener(String data){
        System.out.println("Listener Receive data : " + data +"Successfully");
    }

}
